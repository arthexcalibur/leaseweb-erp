<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return redirect('/servers');
});
Route::resource('/servers', 'App\Http\Controllers\ServerController')->except(['create', 'show', 'update', 'edit']);
Route::resource('/ram_modules', 'App\Http\Controllers\RamModuleController')->except(['index', 'create', 'show', 'update', 'edit']);