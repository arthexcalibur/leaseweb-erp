<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('/servers', 'App\Http\Controllers\Api\ServerController', ['names' => 'server_api'])->except(['create', 'edit', 'update']);
Route::resource('/ram_modules', 'App\Http\Controllers\Api\RamModuleController', ['names' => 'ram_modules_api'])->except(['create', 'edit', 'update']);