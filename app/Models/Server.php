<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['asset_id', 'brand', 'name', 'price'];

    public function ramModules()
    {
        return $this->hasMany(RamModule::class, 'server_id');
    }
}
