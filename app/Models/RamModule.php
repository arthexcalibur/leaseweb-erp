<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RamModule extends Model
{
    use HasFactory;

    protected $hidden = ['created_at', 'updated_at'];

    protected $fillable = ['type', 'size', 'server_id'];

    public function server()
    {
        return $this->belongsTo(Server::class, 'server_id');
    }
}
