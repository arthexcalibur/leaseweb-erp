<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Server;
use App\Models\RamModule;

class RamModuleController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'server_id' => 'required|exists:servers,id',
            'type' => 'required|alpha_num',
            'size' => 'required|integer'
        ]);

        $ramModule = new RamModule($validated);
        $ramModule->save();

        return redirect('/servers')->with('message', 'RAM module added!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ramModule = RamModule::find($id);

        if ($ramModule)
        {
            $server = $ramModule->server;

            if (count($server->ramModules) > 1)
            {
                $ramModule->delete();

                return redirect('/servers')->with('message', 'RAM module deleted!');
            }
            else
            {
                return redirect('/servers')->with('error', 'ASSET ID: ' . $server->asset_id . ' must have atleast 1 RAM module!');
            }
        }
        else
        {
            return redirect('/servers')->with('error', 'RAM module not found!');
        }
    }
}
