<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Server;
use App\Models\RamModule;

class RamModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ramModules = RamModule::all();
        return response()->json($ramModules, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'server_id' => 'required|exists:servers,id',
            'type' => 'required|alpha_num',
            'size' => 'required|integer'
        ]);

        $ramModule = new RamModule($validated);
        $ramModule->save();

        return response()->json($ramModule, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ramModule = RamModule::find($id);

        if ($ramModule)
             return response()->json($ramModule, 200);
         else
             return response()->json(['message' => 'RAM module not found'], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $ramModule = RamModule::find($id);

        if ($ramModule)
        {
            if (count($ramModule->server->ramModules) > 1)
            {
                $ramModule->delete();
                return response()->json(null, 204);
            }
            else
            {
                return response()->json(['message' => 'Server must have atleast one RAM module'], 412); 
            }
        }
        else
        {
            return response()->json(['message' => 'RAM module not found'], 404);
        }
    }
}
