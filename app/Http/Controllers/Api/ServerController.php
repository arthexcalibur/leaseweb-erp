<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Server;
use App\Models\RamModule;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servers = Server::all();
        $servers->load('ramModules');
        return response()->json($servers, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'asset_id' => 'required|integer|unique:servers,asset_id',
            'brand' => 'required|alpha_num',
            'name' => 'required|alpha_num',
            'price' => 'required|numeric|gt:0.01',
            'ram_modules' => 'required|array',
            'ram_modules.*.type' => 'required|string',
            'ram_modules.*.size' => 'required|integer',
        ]);

        $validatedServer = [
            'asset_id' => $validated['asset_id'],
            'brand' => $validated['brand'],
            'name' => $validated['name'],
            'price' => $validated['price']
        ];

        $server = new Server($validatedServer);
        $server->save();

        foreach ($validated['ram_modules'] as $validatedRamModule)
        {
            $validatedRamModule['server_id'] = $server->id;

            $ramModule = new RamModule($validatedRamModule);
            $ramModule->save();
        }

        $server->load('ramModules');

        return response()->json($server, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $server = Server::find($id);

        if ($server)
        {
            $server->load('ramModules');

            return response()->json($server, 200);
        }
         else
         {
            return response()->json(['message' => 'Server not found'], 404);
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $server = Server::find($id);

        if ($server)
        {
            foreach ($server->ramModules as $ramModule)
            {
                $ramModule->delete();
            }

            $server->delete();
            
            return response()->json(null, 204);
        }
        else
        {
            return response()->json(['message' => 'Server not found'], 404);
        }
    }
}
