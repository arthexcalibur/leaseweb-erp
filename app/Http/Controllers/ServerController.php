<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Server;
use App\Models\RamModule;

class ServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('server.index')->with(['servers' => Server::all()]);  
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'asset_id' => 'required|integer|unique:servers,asset_id',
            'brand' => 'required|alpha_num',
            'name' => 'required|alpha_num',
            'price' => 'required|numeric|gt:0.01',
            'ram_modules' => 'required|array',
            'ram_modules.*.type' => 'required|string',
            'ram_modules.*.size' => 'required|integer',
        ]);

        $validatedServer = [
            'asset_id' => $validated['asset_id'],
            'brand' => $validated['brand'],
            'name' => $validated['name'],
            'price' => $validated['price']
        ];

        $server = new Server($validatedServer);
        $server->save();

        foreach ($validated['ram_modules'] as $validatedRamModule)
        {
            $validatedRamModule['server_id'] = $server->id;

            $ramModule = new RamModule($validatedRamModule);
            $ramModule->save();
        }

        return redirect('/servers')->with('message', 'Server saved!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $server = Server::find($id);
        $deleted = false;

        if ($server)
        {
            foreach ($server->ramModules as $ramModule)
            {
                $ramModule->delete();
            }

            $server->delete();
            $deleted = true;
        }

        if ($deleted)
            return redirect('/servers')->with('message', 'Server and RAM modules deleted!');
        else if (!$server)
            return redirect('/servers')->with('error', 'Server ID not found!');
        else 
            return redirect('/servers')->with('error', 'Deletion not successful!');
    }
}
