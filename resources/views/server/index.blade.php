<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="my-4 flex text-center">
                <img src="/public/images/logo.png" alt="logo" title="logo"/>
            </div>

            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif

            @if(session()->has('error'))
                <div class="alert alert-danger">
                    {{ session()->get('error') }}
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-9">
                    <div class="col-12 p-4 bg-light border">
                        <h3>Server List</h3>
                        <div class="row p-2 border border-bottom-non fw-bold">
                            <div class="col-2">AssetID</div>
                            <div class="col-2">Brand</div>
                            <div class="col-2">Name</div>
                            <div class="col-2">Price</div>
                            <div class="col-4 ps-4 border-start">RAM Modules</div>
                        </div>
                        @if(count($servers) < 1)
                        <h6 class="text-center my-4">No Servers found!</h6>
                        @else
                            @foreach($servers as $server)
                                <div class="row p-2 border border-top-0">
                                    <div class="col-2">
                                        {{ $server->asset_id }}
                                        <div>
                                            <form method="POST" action="/servers/{{ $server['id'] }}" onsubmit="return confirm('Are you sure you want to delete this Server?');">
                                                @csrf
                                                @method('DELETE')
                                                <button class="w-100 mt-4 btn btn-danger" type="submit">DELETE</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-2">{{ $server->brand }}</div>
                                    <div class="col-2">{{ $server->name }}</div>
                                    <div class="col-2">{{ $server->price }}</div>
                                    <div class="col-4 ps-4 border-start">
                                        @foreach($server->ramModules as $ramModule)
                                        <div class="row mb-2">
                                            <div class="col-12 badge bg-warning text-start">
                                                <div class="row">
                                                    <div class="col-8">
                                                        <div class="p-1">Type: {{ $ramModule['type'] }}</div>
                                                        <div class="p-1">Size: {{ $ramModule['size'] }}GB</div>
                                                    </div>
                                                    <div class="col-4">
                                                        <form action="/ram_modules/{{ $ramModule['id'] }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this RAM Module?');">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="form-control" type="submit">X</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <hr>
                                        <h6 class="mt-2">Add RAM Modules</h6>
                                        <form action="/ram_modules" method="POST">
                                            @csrf
                                            <div class="input-group mb-3">
                                                <input type="hidden" name="server_id" value="{{ $server->id }}" />
                                                <span class="input-group-text">Type</span>
                                                <input value="{{ $errors->any() && $server->id == old('server_id') ? old('type') : '' }}" type="text" name="type" class="form-control">
                                                <span class="input-group-text">Size</span>
                                                <input value="{{ $errors->any() && $server->id == old('server_id') ? old('size') : '' }}" type="text" name="size" class="form-control">
                                            </div>
                                            <button type="submit" class="form-control btn btn-secondary">Add RAM Module</button>
                                        </form>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="col-3">
                    <div class="col-12 p-4 bg-light border">
                        <form method="POST" action="/servers">
                            @csrf
                            <h3>Add New Server</h3>
                            <label class="form-label" for="asset_id">Asset ID</label><input value="{{ $errors->any() ? old('asset_id') : '' }}" class="form-control" name="asset_id" type="text" id="asset_id" />
                            <label class="form-label" for="brand">Brand</label><input value="{{ $errors->any() ? old('brand') : '' }}" class="form-control" name="brand" type="text" id="brand" />
                            <label class="form-label" for="name">Name</label><input value="{{ $errors->any() ? old('name') : '' }}" class="form-control" name="name" type="text" id="name" />
                            <label class="form-label" for="price">Price</label><input value="{{ $errors->any() ? old('price') : '' }}" class="form-control" name="price" type="text" id="price" />
                            <hr>
                            <h6 class="mt-2">RAM Module</h6>
                            <div class="input-group mb-3">
                                <span class="input-group-text">Type</span>
                                <input type="text" value="{{ $errors->any() ? old('ram_modules.0.type') : '' }}" name="ram_modules[0][type]" class="form-control">
                                <span class="input-group-text">Size</span>
                                <input type="text" value="{{ $errors->any() ? old('ram_modules.0.size') : '' }}" name="ram_modules[0][size]" class="form-control">
                            </div>
                            <hr>
                            <div class="text-center my-2">
                                <button type="submit" class="form-control btn btn-primary">Add Server</button>
                            </div>
                        </form>
                     </div>
                </div>
            </div>
            <small>Laravel v{{ Illuminate\Foundation\Application::VERSION }} (PHP v{{ PHP_VERSION }})</small>
        </div>
    </body>
</html>
