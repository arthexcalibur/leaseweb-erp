<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serverId = DB::table('servers')->insert([
            'asset_id' => '123321',
            'brand' => 'Dell',
            'name' => 'Server 1',
            'price' => rand(500 * 10, 10000 * 10) / 10,
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('ram_modules')->insert([
            'type' => 'DDR4',
            'size' => 8,
            'server_id' => $serverId,
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);
    }
}
